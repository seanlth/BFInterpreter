require 'brainfuck'


describe "is_valid_script" do
	it 'returns true for script [[]]' do
		expect(BfInterpreter.is_valid_script("[[]]")).to eq true
	end

	it 'returns true for script [++[++]]' do
		expect(BfInterpreter.is_valid_script("[++[++]]")).to eq true
	end

	it 'returns true for script [+[][]][[[]]][[]+[++]]' do
		expect(BfInterpreter.is_valid_script("[+[][]][[[]]][[]+[++]]")).to eq true
	end

	it 'returns false for script [][[]]]][[' do
		expect(BfInterpreter.is_valid_script("[][[]]]][[")).to eq false
	end
end

describe "handle_char" do
	it 'returns [0, 1] and mem[0] == 1 for state == [0, 0], char == + and script == [[]]' do
		mem = Array.new(100, 0)

		interp = BfInterpreter.new("[[]]")

		expect(interp.handle_char("+", mem, 0, 0)).to eq([0, 1])
		expect(mem[0]).to eq(1)
	end

	it 'returns [0, 1] and mem[0] == -1 for state == [0, 0], char == - and script == [[]]' do
		mem = Array.new(100, 0)

		interp = BfInterpreter.new("[[]]")

		expect(interp.handle_char("-", mem, 0, 0)).to eq([0, 1])
		expect(mem[0]).to eq(-1)
	end

	it 'returns [1, 1] for state == [0, 0], char == > and script == [[]]' do
		mem = Array.new(100, 0)

		interp = BfInterpreter.new("[[]]")

		expect(interp.handle_char(">", mem, 0, 0)).to eq([1, 1])
		expect(mem[0]).to eq(0)
	end

	it 'returns [0, 1] for state == [1, 0], char == < and script == [[]]' do
		mem = Array.new(100, 0)

		interp = BfInterpreter.new("[[]]")

		expect(interp.handle_char("<", mem, 1, 0)).to eq([0, 1])
		expect(mem[0]).to eq(0)
	end

	it 'returns [0, 3] for state == [0, 0], char == [ and script == [[]]' do
		mem = Array.new(100, 0)

		interp = BfInterpreter.new("[[]]")

		expect(interp.handle_char("[", mem, 0, 0)).to eq([0, 3])
		expect(mem[0]).to eq(0)
	end

	it 'returns [0, 2] for state == [0, 1], char == [ and script == [[]]' do
		mem = Array.new(100, 0)

		interp = BfInterpreter.new("[[]]")

		expect(interp.handle_char("[", mem, 0, 1)).to eq([0, 2])
		expect(mem[0]).to eq(0)
	end

	it 'returns [0, 0] for mem[0] == 1, state == [0, 3], char == ] and script == [[]]' do
		mem = Array.new(100, 0)

		interp = BfInterpreter.new("[[]]")
		mem[0] = 1
		expect(interp.handle_char("]", mem, 0, 3)).to eq([0, 0])
		expect(mem[0]).to eq(1)
	end
end

describe "populate_jump_tables" do
	it 'populates for script [[]]' do
		jump_to_open = Hash.new()
		jump_to_close = Hash.new()

		BfInterpreter.populate_jump_tables("[[]]", jump_to_open, jump_to_close)
		
		expect(jump_to_close[0]).to eq 3
		expect(jump_to_close[1]).to eq 2
		expect(jump_to_open[3]).to eq 0
		expect(jump_to_open[2]).to eq 1
	end
	
	it 'populates for script [[][][[]]]' do
		jump_to_open = Hash.new()
		jump_to_close = Hash.new()

		BfInterpreter.populate_jump_tables("[[][][[]]]", jump_to_open, jump_to_close)
		
		expect(jump_to_close[0]).to eq 9
		expect(jump_to_close[1]).to eq 2
		expect(jump_to_close[5]).to eq 8	
		expect(jump_to_close[6]).to eq 7		
		
		expect(jump_to_open[9]).to eq 0
		expect(jump_to_open[8]).to eq 5
		expect(jump_to_open[7]).to eq 6
		expect(jump_to_open[2]).to eq 1
	end

end


