
class BfInterpreter

	def initialize(script)
		@script = script
		
		# save jump points
		@jump_to_open = Hash.new() 
		@jump_to_closed = Hash.new()

		if BfInterpreter.is_valid_script(script) 	
			BfInterpreter.populate_jump_tables(script, @jump_to_open, @jump_to_closed)
		else
			raise 'An invalid script was passed to the interpreter'
		end
	end

	attr_accessor :script

	# checks if all brackets are matched
	def self.is_valid_script(script)
		program_counter = 0
		count = 0

		while program_counter < script.length
			if script[program_counter] == "[" # count open or close brackets
				count += 1   
			elsif script[program_counter] == "]"  
				count -= 1
			end
			program_counter += 1

			if count < 0 
				return false
			end
		end 

		return count == 0
	end


	# Stores a stack of unmatched open brackets 
	# When a closing bracket is encountered the bracket on the top of the stack is popped off 
	
	def self.populate_jump_tables(script, open_table, close_table)
		stack = Array.new()

		program_counter = 0
		count = 0

		# loop over script 
		while program_counter < script.length
			if script[program_counter] == "[" # count open brackets
				
				# add open bracket to first table
				close_table[program_counter] = nil

				# add to stack 
				stack.push(program_counter)

				count += 1 
			elsif script[program_counter] == "]" 

				# remove bracket on top of the stack 
				open = stack.pop

				# add brackets to table
				close_table[open] = program_counter					
				open_table[program_counter] = open	

				count -= 1
			end
			program_counter += 1
			
		end
	end

	def print_mem(mem)
		mem.each do |cell| 
			print "#{cell} "
		end

		puts ""
	end

	def find_close(program_counter)
		@jump_to_closed[program_counter]
	end

	def find_open(program_counter)
		@jump_to_open[program_counter]
	end

	# handles each instruction in the bf language
	# modifies memory and pointers based on current program instruction
	def handle_char(c, mem, cell_pointer, program_counter)
		case c 
			when "+" 
				mem[cell_pointer] += 1
			when "-"
				mem[cell_pointer] -= 1
			when ">"
				cell_pointer += 1
			when "<"
				cell_pointer -= 1
			when "."
				print mem[cell_pointer].chr
			when ","
				mem[cell_pointer]
			when "["
				if mem[cell_pointer] == 0 
					program_counter = find_close program_counter	
					program_counter -= 1				
				end
			when "]"
				if mem[cell_pointer] != 0
					program_counter = find_open program_counter	
					program_counter -= 1				
				end
		end

		program_counter += 1

		return cell_pointer, program_counter
	end

	# run the script
	def execute
		cell_pointer = 0
		program_counter = 0
		mem = Array.new(100, 0)

		while program_counter < @script.length
			c = @script[program_counter]

			cell_pointer, program_counter = handle_char(c, mem, cell_pointer, program_counter)
		end
	end
end

# print hello world
input = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++."

interpreter = BfInterpreter.new(input)

## execute bf code
interpreter.execute

